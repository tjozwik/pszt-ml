require 'weka'

class WekaTree
  def initialize(data_set)
    file_name = "weka#{object_id}.arff"
    file = File.new(file_name, 'w')

    file.puts "@relation test
@attribute 'buying' {vhigh, high, med, low}
@attribute 'maint' {vhigh, high, med, low}
@attribute 'doors' {2, 3, 4, 5more}
@attribute 'persons' {2, 4, 'more'}
@attribute 'lug_boot' {small, med, big}
@attribute 'safety' {low, med, high}
@attribute 'class' { unacc, acc, good, vgood}
@data"
    data_set.each { |d| file.puts d.values.join(',') }
    file.close

    create_classifier(file_name)

    File.delete(file_name)
  end

  def classify(object)
    @tree.classify(object.serialize)
  end

  private

  def create_classifier(file_name)
    instances = Weka::Core::Instances.from_arff(file_name)
    instances.class_attribute = :class

    @tree = Weka::Classifiers::Trees::J48.new

    @tree.train_with_instances(instances)
  end
end