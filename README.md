
# PSZT - ID3, C4.5

Project compares a couple of algorithms used to build a decision tree.

Implemented algorithms:

- ID3
- C4.5
- WEKA J48 (C4.5)

## Installation

Use the Ruby Version Manager (rvm) to install required JRuby (Ruby on Java Virtual Machine) interpreter:

```bash
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable
rvm install jruby
rvm use jruby
```

Install project dependencies:

```bash
gem install rspec weka
```

## Usage
Run program using the command below in order to compare implemented algorithms:

```bash
ruby project.rb
```

## Authors
- Tomasz Jóźwik
- Tomasz Mazur

