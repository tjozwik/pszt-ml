class DataManager
  def initialize(file_name)
    f = File.open(file_name)

    @data = Enumerator.new do |v|
      f.each_line do |line|
        line = line.chomp.split(',')
        v << {
            buying: line[0],
            maint: line[1],
            doors: line[2],
            persons: line[3],
            lug_boot: line[4],
            safety: line[5],
            class: line[6]
        }
      end
    end
  end

  def method_missing(m, *args, &block)
    @data.send(m, *args, &block)
  end
end