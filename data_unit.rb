class DataUnit
  attr_reader :serialize, :deserialize

  def initialize(data_unit)
    @current = @deserialize = data_unit

    @serialize = data_unit.slice(*(data_unit.keys - [:class]))
  end

  def serialize!
    @current = @serialize
  end

  def deserialize!
    @current = @deserialize
  end

  def to_s
    @current.to_s
  end

  def method_missing(m, *args, &block)
    @current.send(m, *args, &block)
  end
end

class DataTestUnit
  attr_reader :matched

  def initialize(data_unit)
    @data_unit = DataUnit.new(data_unit)

    @matched = false
  end

  def match!
    @matched = true
  end

  def to_s
    @data_unit.to_s
  end

  def method_missing(m, *args, &block)
    @data_unit.send(m, *args, &block)
  end
end