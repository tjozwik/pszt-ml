class Tree
  attr_reader :root

  def initialize(root = nil)
    @root = root
  end

  def expand_tree(attr, tree)
    @root[attr] = tree.root
    tree.root.upper = {
        attribute: attr,
        node: @root
    }
  end

  def classify(object, visit = false, trace = false)
    analyze_tree unless @already_analyzed

    node = @root

    until node.is_a? Leaf
      if node.lower[object[node.value]].nil?
        return recursive_classify(node)
      end

      node.bucket << object if trace

      node = node.lower[object[node.value]]
    end

    if trace
      node.bucket << object

      if node.value == object.deserialize[:class]
        object.match!
      end
    end

    node.visit if visit

    node.value
  end

  def id3(c, r, s)
    @training_objects_set ||= s

    if s.empty?
      raise StandardError.new "Object's set cannot be empty"
    elsif (s.map { |obj| obj[:class] }).uniq.count == 1
      @root = Leaf.new(s.first[:class])
      return
    elsif r.empty?
      @root = Leaf.new((s.sort_by { |obj| s.count { |e| e[:class] == obj[:class] } }).last[:class])
      return
    end

    d_attribute = r.max_by { |obj| inf_gain(obj, s) }
    @root = Node.new(d_attribute)

    d_values = (s.map { |obj| obj[d_attribute] }).uniq

    s_parts = []

    d_values.each do |v|
      s_parts << (s.select { |obj| obj[d_attribute] == v })
    end
    
    s_parts.each_with_index do |v, i|
      lower_tree = Tree.new
      lower_tree.id3(c.clone, r - [d_attribute], v.clone)

      self.expand_tree(d_values[i].clone, lower_tree)
    end
  end

  def c4_5(c, r, s)
    pruning_set_size = s.count / 5
    pruning_set = s[0...pruning_set_size]
    s = s[pruning_set_size..-1]

    id3(c, r, s)
    c4_5_check(pruning_set)
  end

  def [](key)
    @root[key]
  end

  def []=(key, value)
    @root[key] = value
  end

  private

  def c4_5_check(pruning_set)
    @already_analyzed = true

    pruning_set.each do |e|
      classify(DataTestUnit.new(e.deserialize), false, true)
    end

    recursive_prune(@root)

    @already_analyzed = false
  end

  def recursive_prune(node)
    if node.is_a? Leaf
      return
    end

    node.lower.each do |_, v|
      recursive_prune(v)
    end

    already_matched = node.bucket.count { |e| e.matched }
    available_classes = (node.bucket.map { |b| b.deserialize[:class] }).uniq
    most_common_class = available_classes.max_by { |c| (node.bucket.map { |b| b.deserialize[:class] }).count(c) }
    number_of_most_common_class_instances = (node.bucket.map { |b| b.deserialize[:class] }).count(most_common_class)

    if number_of_most_common_class_instances >= already_matched
      leaf = Leaf.new(most_common_class)
      leaf.bucket = node.bucket
      leaf.upper = node.upper

      node.upper[:node].lower[node.upper[:attribute]] = leaf
    end
  end

  def inf_gain(d, s)
    i(s) - inf(d, s)
  end

  def i(s)
    result = 0

    (s.map { |obj| obj[:class] }).uniq.each do |i|
      f = (s.count { |obj| obj[:class] == i }).to_f / s.count
      result += (f * Math.log(f))
    end

    -result
  end

  def inf(d, s)
    result = 0
    j_values = (s.map { |obj| obj[d] }).uniq

    j_values.each do |v|
      result += ((s.count { |obj| obj[d] == v }).to_f / s.count * i(s.select { |obj| obj[d] == v }))
    end

    result
  end

  def recursive_classify(node)
    @@classify_priorities = Hash.new(0)

    deep_search(node)

    (@@classify_priorities.max_by { |_, v| v }).first
  end

  def deep_search(node)
    if node.is_a? Leaf
      @@classify_priorities[node.value] += node.visits
      return
    end

    node.lower.each_value { |v| deep_search(v) }
  end

  def analyze_tree
    @already_analyzed = true

    @training_objects_set.each { |t| classify(t, true) }
  end
end

class Node
  include Comparable

  attr_reader :value
  attr_accessor :lower, :upper, :bucket

  def initialize(value)
    @value = value
    @lower = {}
    @bucket = []
  end

  def <=>(other)
    @value <=> other.value
  end

  def [](key)
    @lower[key]
  end

  def []=(key, value)
    @lower[key] = value
  end
end

class Leaf < Node
  attr_reader :visits

  def initialize(value)
    super(value)

    @visits = 0
  end

  def visit
    @visits += 1
  end
end