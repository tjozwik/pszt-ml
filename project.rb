require_relative 'cross_validation'
require_relative 'tree'
require_relative 'weka_classifier'

# Tomasz Jóźwik
# Tomasz Mazur

test_cases = [3, 5, 7, 10]
number_of_repeats = 15

puts "SETS_REPEAT ID3 C45 WEKA_J48"

test_cases.each do |number_of_sets|
  number_of_repeats.times do |repeat_counter|
    c = CrossValidation.new(number_of_sets,'car.data')

    sets = []

    c.each { |s| sets << s }

    sets.each do |validation_set|
      matched_1, matched_2, weka_matched, counter = 0, 0, 0, 0

      objects = (sets - [validation_set]).flatten

      weka_tree = WekaTree.new(objects)

      tree_1 = Tree.new
      tree_1.id3((objects.map { |obj| obj[:class] }).uniq,
                 objects.first.map(&:first)[0..-2],
                 objects)

      tree_2 = Tree.new
      tree_2.c4_5((objects.map { |obj| obj[:class] }).uniq,
                  objects.first.map(&:first)[0..-2],
                  objects)

      validation_set.each do |v|
        counter += 1

        if v[:class] == tree_1.classify(v.serialize)
          matched_1 += 1
        end

        if v[:class] == tree_2.classify(v.serialize)
          matched_2 += 1
        end

        if v[:class] == weka_tree.classify(v)
          weka_matched += 1
        end
      end

      puts "#{number_of_sets}_#{repeat_counter} #{matched_1.to_f / counter} #{matched_2.to_f / counter} #{weka_matched.to_f / counter}"
    end
  end
end
