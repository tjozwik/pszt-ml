require_relative '../tree'

describe Node do
  it 'creates valid object' do
    node = Node.new(42)

    expect(node.value).to eq 42
    expect(node.upper).to eq nil
    expect(node.lower.count).to eq 0
  end

  it 'can be compared to other Node' do
    node_1 = Node.new(42)
    node_2 = Node.new(20)

    expect(node_1 < node_2).to be false
    expect(node_1 > node_2).to be true
  end
end

describe Leaf do
  it 'creates valid object' do
    leaf = Leaf.new(:value)

    expect(leaf.value).to be :value
    expect(leaf.visits).to be_zero
  end

  it 'can be visited' do
    leaf = Leaf.new(:value)

    leaf.visit

    expect(leaf.value).to be :value
    expect(leaf.visits).to eq 1
  end
end

describe Tree do
  it 'creates valid object' do
    node = Node.new(42)

    tree = Tree.new(node)

    expect(tree.root).to be node
    expect(tree.root.value).to eq 42
    expect(tree.root.upper).to eq nil
  end

  it 'expands tree' do
    node_1 = Node.new(42)
    node_2 = Node.new(18)

    tree_1 = Tree.new(node_1)
    tree_2 = Tree.new(node_2)
    tree_1.expand_tree(:new_tree_arg, tree_2)

    expect(tree_1.root.lower.count).to eq 1
    expect(tree_1[:new_tree_arg]).to be node_2
    expect(tree_1[:new_tree_arg].value).to eq 18
    expect(tree_1[:new_tree_arg].upper[:attribute]).to eq :new_tree_arg
    expect(tree_1[:new_tree_arg].upper[:node]).to eq node_1
  end
end