require_relative '../cross_validation'

describe CrossValidation do
  it 'creates a valid object' do
    cross_validator = CrossValidation.new(5, 'car.data')

    expect(cross_validator).not_to be_nil
  end

  it 'creates proper number of sets' do
    cross_validator = CrossValidation.new(10, 'car.data')

    expect(cross_validator.count).to eq 10
  end

  it 'splits data set' do
    cross_validator = CrossValidation.new(5, 'car.data')

    data_sets = cross_validator.first(2)

    expect(data_sets.first).not_to eq data_sets.last
  end
end