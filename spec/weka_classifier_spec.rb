require_relative '../cross_validation'
require_relative '../weka_classifier'

describe WekaTree do
  before :all do
    @cross_validator = CrossValidation.new(2, 'car.data')
  end

  it 'creates a valid object' do
    weka_tree = WekaTree.new(@cross_validator.first)

    expect(weka_tree).not_to be_nil
  end

  it 'classifies objects' do
    data = @cross_validator.first

    classified_data = data.first

    weka_tree = WekaTree.new(data)

    result = weka_tree.classify(classified_data)

    expect(result).to be_a String
    expect(result).not_to be ''
  end
end