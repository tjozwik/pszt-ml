require_relative '../data_manager'

describe DataManager do
  it 'creates a valid object' do
    data_manager = DataManager.new('car.data')

    expect(data_manager).not_to be_nil
  end

  it 'serves data' do
    data_manager = DataManager.new('car.data')

    first_data_hash = data_manager.first
    data_hash_set = data_manager.first(10)

    expect(first_data_hash).to be_a Hash
    expect(data_hash_set).to be_a Array
    expect(data_hash_set.uniq.count).to eq 10
  end
end