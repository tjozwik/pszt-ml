require_relative '../data_unit'

describe DataUnit do
  before :all do
    @test_hash = {
        test_value_1: '1',
        test_value_2: '2',
        class: 'test'
    }
  end

  it 'creates a valid object' do
    data_unit = DataUnit.new(@test_hash)

    expect(data_unit.to_s).to eq @test_hash.to_s
  end

  it 'can be serialized' do
    data_unit = DataUnit.new(@test_hash)

    data_unit.serialize!

    expect(data_unit[:class]).to be_nil
    expect(data_unit.count).to eq 2
  end

  it 'can be deserialized' do
    data_unit = DataUnit.new(@test_hash)

    data_unit.serialize!
    data_unit.deserialize!

    expect(data_unit[:class]).to eq 'test'
    expect(data_unit.count).to eq 3
  end

  it 'can return specified form' do
    data_unit = DataUnit.new(@test_hash)

    expect(data_unit.serialize.count).to eq 2
    expect(data_unit.deserialize.count).to eq 3
  end
end

describe DataTestUnit do
  before :all do
    @test_hash = {
        test_value_1: '1',
        test_value_2: '2',
        class: 'test'
    }
  end

  it 'creates a valid object' do
    data_test_unit = DataTestUnit.new(@test_hash)

    expect(data_test_unit.to_s).to eq @test_hash.to_s
    expect(data_test_unit.matched).to be_falsey
  end

  it 'can be matched' do
    data_test_unit = DataTestUnit.new(@test_hash)

    data_test_unit.match!

    expect(data_test_unit.matched).to be_truthy
  end
end