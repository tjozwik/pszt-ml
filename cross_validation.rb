require_relative 'data_manager'
require_relative 'data_unit'

class CrossValidation
  attr_reader :data

  def initialize(number_of_sets, file_name)
    data_manager = DataManager.new(file_name)

    @data = []
    data_manager.each { |d| @data << d }
    @data.shuffle!

    @data.map! { |d| DataUnit.new(d) }

    smaller_set_capacity = @data.count / number_of_sets
    larger_set_capacity = smaller_set_capacity + 1
    larger_sets_count = @data.count % number_of_sets
    larger_sets_capacity = larger_sets_count * larger_set_capacity

    larger_sets = @data[0...larger_sets_capacity].each_slice(larger_set_capacity).to_a
    smaller_sets = @data[larger_sets_capacity..-1].each_slice(smaller_set_capacity).to_a

    sets = larger_sets + smaller_sets

    @data_enumerator = Enumerator.new do |y|
      sets.each { |s| y << s }
    end
  end

  def method_missing(m, *args, &block)
    @data_enumerator.send(m, *args, &block)
  end
end